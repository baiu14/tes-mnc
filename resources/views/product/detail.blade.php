@extends('layout.mylayout')

@section('title', 'Product')
@section('pageHeader','Detail Product')


@section('breadcumb')
	 <ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li> 
		<li class="breadcrumb-item active">Form Product</li>
	</ol>
@endsection
@section('content')
	<div class="col-12">
		<div class="row form-group">
			<div class="col-6">
				<label class="col-12">Title</label>
				<div class="col-12">
					<input class="form-control" disabled="disabled" value="{{ ( ( empty( $row ) ) ? '' : $row->title ) }}">
				</div>
			</div>
			<div class="col-6">
				<label class="col-12">Category</label>
				<div class="col-12">
					<input class="form-control" disabled="disabled" value="{{ $row->category_id->title }}">
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-12">
				<label class="col-12">Description</label>
				<div class="col-12">
					<textarea class="form-control" disabled="disabled">{{ ( ( empty( $row ) ) ? '' : $row->description ) }}</textarea>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-6">
				<label class="col-12">Available Color</label>
				<div class="col-12">
					@foreach( explode( ",", $row->color ) as $col )
					<label class="badge badge-primary">{{ $col }}</label>
					@endforeach
				</div>
			</div>
			<div class="col-6">
				<label class="col-12">Available Size</label>
				<div class="col-12">
					@foreach( explode( ",", $row->size ) as $si )
					<label class="badge badge-primary">{{ $si }}</label>
					@endforeach
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-6">
				@foreach( $row->images as $img )
				{{ url( $img->path ) }}
				@endforeach
			</div>
		</div>
		<div class="row form-group">
			<div class="col-12">
				<label class="col-12">Variant</label>
				<div class="col-12">
					@foreach( $row->variant as $var )
					<li>{{ $var->name }} : {{ number_format($var->price, 2) }}</li>
					@endforeach
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-12">
				<a href="<?php echo route( 'productsTable' );?>" class="btn btn-sm btn-danger">Cancel</a>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script>
	$(document).ready(function(){
		let arr_color = [];
		$( 'button.addColor' ).on( 'click', function(){
			let color = $( 'input#color' ).val();
			if ( $.inArray( color, arr_color ) ) {
				let el = `
					<div class="col-3"><label class="badge badge-primary">${color}</label></div>
				`;
				arr_color.push( color );
				$( 'input[name="color"]' ).val( arr_color.toString() );
				$( 'input#color' ).val( '' );
				$( '#colorAvail' ).append( el );
			}
		});

		$( 'button#save' ).on( 'click', function(){
			let data = {
				_token: $( 'input[name="_token"]' ).val(),
				id: $( 'input[name="id"]' ).val(),
				title: $( 'input[name="title"]' ).val(),
				description: $( 'input[name="description"]' ).val(),
				color: arr_color.toString(),
			}
			$.ajax({
				url: $( '#faormData' ).attr( "action" ),
				type: "post",
				data: data ,
				success: function (response) {

				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
		});
	})
	</script>
	@endpush
