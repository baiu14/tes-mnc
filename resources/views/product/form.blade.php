@extends('layout.mylayout')

@section('title', 'Product')
@section('pageHeader','Form Product')


@section('breadcumb')
	 <ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li> 
		<li class="breadcrumb-item active">Form Product</li>
	</ol>
@endsection
@section('content')
	<form id="formData" action="{{ $saveUrl }}" method="POST">
		@csrf
		<div class="col-12">
			<div class="row form-group">
				<div class="col-6">
					<label class="col-12">Title</label>
					<div class="col-12">
						<input type="hidden" name="id" value="{{ ( ( empty( $row ) ) ? '' : $row->id ) }}">
						<input type="text" name="title" class="form-control" value="{{ ( ( empty( $row ) ) ? '' : $row->title ) }}">
					</div>
					<div class="col-12" id="error-title"></div>
				</div>
				<div class="col-6">
					<label class="col-12">Category</label>
					<div class="col-12">
						<select name="category" class="form-control">
							<option value=""> -- Category -- </option>
							@foreach( $categories as $cat )
							<option value="{{ $cat->id }}">{{ $cat->title}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-12" id="error-category"></div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-12">
					<label class="col-12">Description</label>
					<div class="col-12">
						<textarea name="description" class="form-control">{{ ( ( empty( $row ) ) ? '' : $row->description ) }}</textarea>
					</div>
					<div class="col-12" id="error-description"></div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-6">
					<div class="col-12">
						<label class="col-12">Available Color</label>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-10">
								<input type="text" id="color" class="form-control">
								<input type="hidden" name="color">
							</div>
							<div class="col-2">
								<button type="button" class="btn btn-sm btn-info addColor">Add</button>
							</div>
						</div>
					</div>
					<div class="col-12" id="error-color"></div>
					<div class="col-12">
						<div id="colorAvail" class="row">
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="col-12">
						<label class="col-12">Available Size</label>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-10">
								<input type="text" id="size" class="form-control">
								<input type="hidden" name="size">
							</div>
							<div class="col-2">
								<button type="button" class="btn btn-sm btn-info addSize">Add</button>
							</div>
						</div>
					</div>
					<div class="col-12" id="error-size"></div>
					<div class="col-6">
						<div class="col-12">
							<div id="sizeAvail" class="row">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-12">Images</label>
				<div class="col-6">
					<input type="file" name="image[]" accept=".jpg,.jpeg,.png" class="form-control" multiple="multiple">
				</div>
			</div>
			<div class="row form-group">
				<div class="col-12">
					<a href="<?php echo route( 'productsTable' );?>" class="btn btn-sm btn-danger">Cancel</a>
					<button type="button" id="save" class="btn btn-sm btn-success">Save</button>
				</div>
			</div>
		</div>
	</form>
@endsection

@push('scripts')
	<script>
	$(document).ready(function(){
		let arr_color = [];
		let arr_size = [];
		$( 'button.addColor' ).on( 'click', function(){
			let color = $( 'input#color' ).val();
			if ( $.inArray( color, arr_color ) ) {
				let el = `
					<div class="col-3"><label class="badge badge-primary">${color}</label></div>
				`;
				arr_color.push( color );
				$( 'input[name="color"]' ).val( arr_color.toString() );
				$( 'input#color' ).val( '' );
				$( '#colorAvail' ).append( el );
			}
		});

		$( 'button.addSize' ).on( 'click', function(){
			let size = $( 'input#size' ).val();
			if ( $.inArray( size, arr_size ) ) {
				let el = `
					<div class="col-3"><label class="badge badge-primary">${size}</label></div>
				`;
				arr_size.push( size );
				$( 'input[name="size"]' ).val( arr_size.toString() );
				$( 'input#size' ).val( '' );
				$( '#sizeAvail' ).append( el );
			}
		});

		$( 'button#save' ).on( 'click', function(){
			// let data = {
			// 	_token: $( 'input[name="_token"]' ).val(),
			// 	id: $( 'input[name="id"]' ).val(),
			// 	title: $( 'input[name="title"]' ).val(),
			// 	description: $( 'input[name="description"]' ).val(),
			// 	color: arr_color.toString(),
			// 	size: arr_size.toString(),
			// }

			var formData = new FormData( $( 'form#formData' )[0] );
			formData.append( color, arr_color.toString() );
			formData.append( size, arr_size.toString() );
			$.ajax({
				type:'POST',
				url: $( '#formData' ).attr( "action" ),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: (resp) => {
					if ( ! resp.success ) {
						$.each( resp.message, function( x, y){
							$( '#error-'+ x ).html( `<span  class="text-danger">${y}</span>` );
						})
					} else{
						window.location = "{{ route( 'productsTable' ) }}";
					}
				},
				error: function(data){
					alert( textStatus + ' : ' + errorThrown );
					console.log( textStatus, errorThrown );
				}
			});
		});
	})
	</script>
	@endpush
