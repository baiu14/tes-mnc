@extends('layout.mylayout')

@section('title', 'Product')
@section('pageHeader','List Products')


@section('breadcumb')
	 <ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li> 
		<li class="breadcrumb-item active">List Categories</li>
	</ol>
@endsection
@section('content')
	<div class="form-group">
		<a href="{{ $addUrl }}" class="btn btn-sm btn-info">Create</a>
	</div>
	<table class="table table-bordered" id="users-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Action</th>
				<th>Title</th>
				<th>Description</th>
				<th>Variant</th>
			</tr>
		</thead>
	</table>
@endsection

@push('scripts')
	<script>
	$(function() {
		$('#users-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ $dataUrl }}',
			columns: [
				{ data: 'no', name: '#'},
				{ data: 'action', name: 'Action'},
				{ data: 'title', name: 'Title' },
				{ data: 'color', name: 'Color' },
				{ data: 'variant', name: 'Variant' },
			]
		});
	});
	</script>
	@endpush
