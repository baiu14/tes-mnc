@extends('layout.mylayout')

@section('title', 'Category')
@section('pageHeader','List Categories')


@section('breadcumb')
	 <ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li> 
		<li class="breadcrumb-item active">List Categories</li>
	</ol>
@endsection
@section('content')
	<div class="form-group">
		<a href="{{ $addUrl }}" class="btn btn-sm btn-info">Create</a>
	</div>
	<table class="table table-bordered" id="users-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Action</th>
				<th>Title</th>
				<th>Description</th>
			</tr>
		</thead>
	</table>
@endsection

@push('scripts')
	<script>
		$(function() {
			$('#users-table').DataTable({
				processing: true,
				serverSide: true,
				ajax: '{{ $dataUrl }}',
				columns: [
					{ data: 'no', name: '#'},
					{ data: 'action', name: 'Action'},
					{ data: 'title', name: 'Title' },
					{ data: 'description', name: 'Description' },
				]
			});
		});

		$(document).ready(function(){
			$( document ).on( 'click', 'button.removeData', function(){
				$.ajax({
					url: $( this ).attr( "act" ),
					type: "get",
					success: function (resp) {
						if ( ! resp.success ) {
							alert( resp.message )
						} else {
							window.location = "{{ route( 'categoriesTable' ) }}";
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert( textStatus + ' : ' + errorThrown );
						console.log( textStatus, errorThrown );
					}
				});
			});
		});
	</script>
	@endpush
