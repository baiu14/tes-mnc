@extends('layout.mylayout')

@section('title', 'Category')
@section('pageHeader','Form Category')


@section('breadcumb')
	 <ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li> 
		<li class="breadcrumb-item active">List Categories</li>
	</ol>
@endsection
@section('content')
	<form id="faormData" action="{{ $saveUrl }}">
		@csrf
		<div class="col-12">
			<div class="row form-group">
				<div class="col-6">
					<label class="col-12">Title</label>
					<div class="col-12">
						<input type="hidden" name="id" value="{{ ( ( empty( $row ) ) ? '' : $row->id ) }}">
						<input type="text" name="title" class="form-control" value="{{ ( ( empty( $row ) ) ? '' : $row->title ) }}">
					</div>
					<div class="col-12" id="error-title"></div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-12">
					<label class="col-12">Description</label>
					<div class="col-12">
						<textarea name="description" class="form-control">{{ ( ( empty( $row ) ) ? '' : $row->description ) }}</textarea>
					</div>
					<div class="col-12" id="error-description"></div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-12">
					<a href="<?php echo route( 'categoriesTable' );?>" class="btn btn-sm btn-danger">Cancel</a>
					<button type="button" id="save" class="btn btn-sm btn-success">Save</button>
				</div>
			</div>
		</div>
	</form>
@endsection

@push('scripts')
	<script>
	$(document).ready(function(){
		$( 'button#save' ).on( 'click', function(){
			let data = {
				_token: $( 'input[name="_token"]' ).val(),
				id: $( 'input[name="id"]' ).val(),
				title: $( 'input[name="title"]' ).val(),
				description: $( 'textarea[name="description"]' ).val(),
			}
			$.ajax({
				url: $( '#faormData' ).attr( "action" ),
				type: "post",
				data: data ,
				success: function (resp) {
					if ( ! resp.success ) {
						$.each( resp.message, function( x, y){
							$( '#error-'+ x ).html( `<span  class="text-danger">${y}</span>` );
						})
					} else {
						window.location = "{{ route( 'categoriesTable' ) }}";
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert( textStatus + ' : ' + errorThrown );
					console.log( textStatus, errorThrown );
				}
			});
		});
	});
	</script>
	@endpush
