@extends('layout.mylayout')

@section('title', 'Product Variant')
@section('pageHeader','Form Product Variant')


@section('breadcumb')
	 <ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li> 
		<li class="breadcrumb-item active">Form Product Variant</li>
	</ol>
@endsection
@section('content')
	<form id="faormData" action="{{ $saveUrl }}" method="POST">
		@csrf
		<div class="col-12">
			<div class="row form-group">
				<div class="col-6">
					<label class="col-12">Product</label>
					<div class="col-12">
						<input type="hidden" name="id" value="{{ ( ( empty( $row ) ) ? '' : $row->id ) }}">
						<input type="hidden" name="product_id" value="{{ ( ( empty( $product ) ) ? '' : $product->id ) }}">
						<input type="hidden" name="product" value="{{ ( ( empty( $product ) ) ? '' : $product->title ) }}">
						<label class="form-control">{{ $product->title }}</label>
					</div>
				</div>
				<div class="col-6">
					<label class="col-12">Size</label>
					<div class="col-12">
						<select name="size" class="form-control">
							<option value=""> -- Size  -- </option>
							@foreach( $size as $si )
							<option value="{{ $si }}">{{ $si }}</option>
							@endforeach
						</select>
					</div>
					@if ($errors->has('size'))
						<span class="text-danger">{{ $errors->first('size') }}</span>
                	@endif
				</div>
			</div>
			<div class="row form-group">
				<div class="col-6">
					<label class="col-12">Color</label>
					<div class="col-12">
						<div class="row">
							@foreach( $color as $col )
							<div class="col-4">
								<input type="checkbox" name="color[]" value="{{ $col }}"> <label>{{ $col }}</label>
							</div>
							@endforeach
						</div>
					</div>
					@if ($errors->has('color'))
						<span class="text-danger">{{ $errors->first('color') }}</span>
                	@endif
				</div>
				<div class="col-6">
					<label class="col-12">Price</label>
					<div class="col-12">
						<input type="text" name="price" class="form-control">
					</div>
					@if ($errors->has('price'))
						<span class="text-danger">{{ $errors->first('price') }}</span>
                	@endif
				</div>
			</div>
			<div class="row form-group">
				
			</div>
			<div class="row form-group">
				<div class="col-12">
					<a href="<?php echo route( 'productsTable' );?>" class="btn btn-sm btn-danger">Cancel</a>
					<button class="btn btn-sm btn-success">Save</button>
				</div>
			</div>
		</div>
	</form>
@endsection

@push('scripts')
	<script>
	$(document).ready(function(){
	})
	</script>
	@endpush
