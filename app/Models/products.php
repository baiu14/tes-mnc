<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class products extends Model {
	use HasFactory;
	protected $fillable = ['title', 'category', 'description', 'color', 'size'];

	public function variant() {
		return $this->hasMany(Product_variants::class, 'product_id');
	}

	public function images() {
		return $this->hasMany(Product_images::class, 'product_id');
	}

	public function category_id() {
		return $this->belongsTo(Categories::class, 'category');
	}
}
