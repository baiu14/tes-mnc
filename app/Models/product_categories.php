<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product_categories extends Model
{
	use HasFactory;

	public function product_id()
	{
		return $this->belongsTo(Products::class);
	}

	public function category_id()
	{
		return $this->belongsTo(category_id::class);
	}

	function public product_variants(){
		return $this->hasMany(Product_variant::class); 
	}
}
