<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product_variants extends Model {
	use HasFactory;
	protected $fillable = [ 'name', 'product_id', 'size', 'price', 'color'];
	public $timestamps = false;

	public function product_id() {
		return $this->belongsToMany(Products::class);
	}
}
