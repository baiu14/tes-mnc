<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Products;
use App\Models\Product_images;
use Validator;

class ProductsController extends Controller {
	public function index() {

	}

	/* view function */
	public function table() {
		$addUrl = route( 'productsCreate' );
		$dataUrl = route( 'productsData' );
		return view( 'product.list', compact( 'addUrl', 'dataUrl' ) );
	}

	public function detail($id) {
		$row = Products::find($id);
		return view( 'product.detail', compact( 'row' ) );
		$row = [];
		$variant = '';
		if ( $query->variant->count() >= 1 ) {
			$variant = '<ul>';
			foreach ( $query->variant as $key => $var ) {
					$variant .= '<li class="mb-1">' . $var->name . ': ' . $var->price . ' <a href="' . route( 'productVariantEdit', $var->id ) . '" class="btn btn-sm btn-warning">Edit</a><a href="' . route( 'productVariantDelete', $var->id ) . '" class="btn btn-sm btn-danger ml-1">remove</a></li>';
			}
			$variant .= '</ul>';
		}
		$color = '<ul>';
		foreach ( explode( ",", $query->color ) as $key => $col ) {
			$color .= '<li>' . $col . '</li>';
		}
		$color .= '</ul>';
		$row[] = [
			'id' => '',
			'title' => $query->title,
			'category' => $query->category_id->title,
			'color' => $color,
			'variant' => $variant,
		];
		return view( 'product.detail', compact( 'row' ) );
	}
	
	public function create() {
		$row = [];
		$saveUrl = route( 'productsSave' );
		$categories = Categories::all();
		return view( 'product.form', compact( 'row', 'saveUrl', 'categories' ) );
	}

	public function edit( $id ) {
		$row = Categories::where( 'id', $id )->first();
		$saveUrl = route( 'productsSave' );
		return view( 'product.form', compact( 'row', 'saveUrl' ) );
	}

	/* form table web */
	public function data() {
		$query = Products::all();
		$data = [];
		$no = 1;
		foreach ( $query as $key => $row ) {
			$variant = '
				<a href="' . route( 'productVariantCreate', $row->id ) . '" class="btn btn-sm btn-warning">Add</a>
			';
			if ( $row->variant->count() >= 1 ) {
				$variant .= '<ul>';
				foreach ( $row->variant->all() as $key => $var ) {
					$variant .= '<li class="mb-1">' . $var->name . ': ' . $var->price . ' <a href="' . route( 'productVariantEdit', $var->id ) . '" class="btn btn-sm btn-warning">Edit</a><a href="' . route( 'productVariantDelete', $var->id ) . '" class="btn btn-sm btn-danger ml-1">remove</a></li>';
				}
				$variant .= '</ul>';
			}
			$color = '<ul>';
			foreach ( explode( ",", $row->color ) as $key => $col ) {
				$color .= '<li>' . $col . '</li>';
			}
			$color .= '</ul>';
			$data[] = [
				'action' => '
					<a href="' . route( 'productsEdit', $row->id ) . '" class="btn btn-sm btn-warning">Edit</a>
					<a href="' . route( 'productsDetail', $row->id ) . '" class="btn btn-sm btn-success">Detail</a>
					<a href="' . route( 'productsDelete', $row->id ) . '" class="btn btn-sm btn-danger">Remove</a>
				',
				'no' => $no,
				'title' => $row->title,
				'color' => $color,
				'variant' => $variant,
			];
			$no++;
		}
		$response = [
			'success' => true,
			'data'    => $data,
		];
		return response()->json($response, 200);
	}

	/* Api */
	public function save( Request $request ) {
		$data_save = [
			'title' => $request->title,
			'category' => $request->category,
			'description' => $request->description,
			'color' => $request->color,
			'size' => $request->size,
		];
		$validator = Validator::make(
			$data_save,
			[
				'title' => 'required',
				'category' => 'required',
				'description' => 'required',
				'color' => 'required',
				'size' => 'required',
			]
		);
		if ( $validator->fails() ) {
			$response = [
				'success' => false,
				'message'  => $validator->errors(),
			];
			return response()->json($response, 200);
		} else {
			if ( empty( $request->id ) ) {
				$newProduk = Products::create( $data_save );
				$productId = $newProduk->id;
			} else {
				Products::where( 'id', $request->id )->update( $data_save );
				$productId = $request->id;
			}
			// $validatedData = $request->validate([
			// 	'image' => 'mimes:jpg,png,jpeg|max:2048',
			// ]);
			foreach ( $request->file('image') as $key => $img) {
				$path = $img->store('public/images/products' );
				$image_save = [
					'product_id' => $productId,
					'variant_id' => "",
					'path' => $path
				];
				Product_images::create( $image_save );
			}
			$response = [
				'success' => true,
				'message'    => 'Data Saved !',
			];
			return response()->json($response, 200);
		}
	}

	public function delete( $id ) {
		if ( empty( $id ) ) {
			$response = [
				'success' => false,
				'message'  => 'failed to delete !',
			];
			return response()->json($response, 200);
		} else {
			Products::where( 'id', $id )->delete();
			$response = [
				'success' => true,
				'message'  => 'data deleted !',
			];
			return response()->json($response, 200);
		}
	}

	public function get_detail( $id ) {
		if ( empty( $id ) ) {
			$response = [
				'success' => false,
				'message'  => 'failed to get data !',
				'data' => '',
			];
			return response()->json($response, 200);
		} else {
			$row = Products::where( 'id', $id )->first();
			$data = [
				'name' => $row->title,
				'category' => $row->category_id,
				'description' => $row->description,
				'images' => url( $row->images ),
				'color' => $row->color,
				'size' => $row->size,
			];
			foreach ( $row->variant as $key => $var ) {
				$data['variant'][] = $var->name . ' : ' . number_format( $var->price, 2 );
			}
			$response = [
				'success' => true,
				'message'  => 'data grabed !',
				'data' => $row,
			];
			return response()->json($response, 200);
		}
	}

	public function get_all(){
		$query = Products::all();
		$date = [];
		foreach ( $query as $key => $cat ) {
			$data[$key] = [
				'name' => $cat->title,
				'category' => $cat->category_id->title,
				'description' => $cat->description,
				'color' => $cat->color,
				'size' => $cat->size,
			];
			$variant = [];
			foreach ( $cat->variant as $key => $var ) {
				$variant[] = $var->name . ' : ' . number_format( $var->price, 2 );
			}

			$data[$key]['variant'] = $variant;
			// dd( $cat->images );
			$image = [];
			foreach ( $cat->images as $key => $im ) {
				$image[] = url( $im->path );
			}
			$data[$key]['images'] = implode(",", $image );

		}
		$response = [
			'success' => true,
			'data'    => $data,
			'message' => 'Data Grabed !'
		];
		return response()->json($response, 200);
	}

	public function get_filter($category){
		$query = Categories::where('title', $category )->first();
		$data = [];
		foreach ( $query->products as $key => $pro ) {
			$row = [
				'name' => $pro->title,
				'category' => $query->title,
				'description' => $pro->description,
				'color' => $pro->color,
				'size' => $pro->size,
			];
			$variant = [];
			foreach ( $pro->variant as $key => $var ) {
				$variant[] = $var->name . ' : ' . number_format( $var->price, 2 );
			}
			$row['variant'] = $variant;
			$image = [];
			foreach ( $pro->images as $key => $im ) {
				$image[] = url( $im->path );
			}
			$row['images'] = implode(",", $image );
			$data[] = $row;
		}
		$response = [
			'success' => true,
			'data'    => $data,
			'message' => 'Data Grabed !'
		];
		return response()->json($response, 200);
	}

	public function get_search($keyword){
		$query = Products::where('title','LIKE','%' . $keyword . '%')->orWhere('description','LIKE','%' . $keyword . '%')->get();
		$data = [];
		foreach ( $query as $key => $pro ) {
			$row = [
				'name' => $pro->title,
				'category' => $pro->category_id->title,
				'description' => $pro->description,
				'color' => $pro->color,
				'size' => $pro->size,
			];
			$variant = [];
			foreach ( $pro->variant as $key => $var ) {
				$variant[] = $var->name . ' : ' . number_format( $var->price, 2 );
			}
			$row['variant'] = $variant;
			$image = [];
			foreach ( $pro->images as $key => $im ) {
				$image[] = url( $im->path );
			}
			$row['images'] = implode(",", $image );
			$data[] = $row;
		}
		$response = [
			'success' => true,
			'data'    => $data,
			'message' => 'Data Grabed !'
		];
		return response()->json($response, 200);
	}
}
