<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use Validator;

class CategoriesController extends Controller {
	public function index() {

	}

	/* view function*/
	public function table() {
		$addUrl = route( 'categoriesCreate' );
		$dataUrl = route( 'categoriesData' );
		return view( 'category.list', compact( 'addUrl', 'dataUrl' ) );
	}
	
	public function create() {
		$row = [];
		$saveUrl = route( 'categoriesSave' );
		return view( 'category.form', compact( 'row', 'saveUrl' ) );
	}

	public function edit($id) {
		$row = Categories::where( 'id', $id )->first();
		$saveUrl = route( 'categoriesSave' );
		return view( 'category.form', compact( 'row', 'saveUrl' ) );
	}

	/* for table web */
	public function data() {
		$query = Categories::all();
		$data = [];
		$no = 1;
		foreach ( $query as $key => $row ) {
			$data[] = [
				'action' => '
					<a href="' . route( 'categoriesEdit', $row->id ) . '" class="btn btn-sm btn-warning">Edit</a>
					<button type="button" act="' . route( 'categoriesDelete', $row->id ) . '" class="btn btn-sm btn-danger removeData">Remove</button>
				',
				'no' => $no,
				'title' => $row->title,
				'description' => $row->description,
			];
			$no++;
		}
		$response = [
			'success' => true,
			'data'    => $data,
		];
		return response()->json($response, 200);
	}

	/* Api */
	public function save( Request $request ) {
		$data_save = [
			'title' => $request->title,
			'description' => $request->description,
		];
		$validator = Validator::make(
			$data_save,
			[
				'title' => 'required',
				'description' => 'required'
			]
		);
		if ( $validator->fails() ) {
			$response = [
				'success' => false,
				'message'  => $validator->errors(),
			];
			return response()->json($response, 200);
		} else {
			if ( empty( $request->id ) ) {
				Categories::create( $data_save );
			} else {
				Categories::where( 'id', $request->id )->update( $data_save );		
			}
			$response = [
				'success' => true,
				'message'  => 'data saved !',
			];
			return response()->json($response, 200);
		}
	}

	public function delete( $id ) {
		if ( empty( $id ) ) {
			$response = [
				'success' => false,
				'message'  => 'failed to delete !',
			];
			return response()->json($response, 200);
		} else {
			Categories::where( 'id', $id )->delete();
			$response = [
				'success' => true,
				'message'  => 'data deleted !',
			];
			return response()->json($response, 200);
		}
	}

	public function get_detail( $id ) {
		if ( empty( $id ) ) {
			$response = [
				'success' => false,
				'message'  => 'failed to get data !',
				'data' => '',
			];
			return response()->json($response, 200);
		} else {
			$row = Categories::where( 'id', $id )->first();
			$response = [
				'success' => true,
				'message'  => 'data grabed !',
				'data' => $row,
			];
			return response()->json($response, 200);
		}
	}

	public function get_all() {
		$data = Categories::find();
		$response = [
			'success' => true,
			'message'  => 'data grabed !',
			'data' => $data,
		];
		return response()->json($response, 200);
	}
}
