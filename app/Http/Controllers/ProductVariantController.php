<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Product_variants;
use Validator;

class ProductVariantController extends Controller {
	public function index() {
		dd("tes");
	}
    
	public function create( $product_id ) {
		$row = [];
		$saveUrl = route( 'productVariantSave' );
		$product = Products::where( 'id', $product_id )->first();
		$color = explode( ",", $product->color );
		$size = explode( ",", $product->size );
		return view( 'product_variant.form', compact( 'row', 'saveUrl', 'product', 'color', 'size' ) );
	}

	public function edit( $id ) {
		$row = Product_variants::where( 'id', $id )->products_id;
		// $row_p = Products::where( 'id', $id )->first()->products_id;
		dd( $row );
		$saveUrl = route( 'productVariantSave' );
		return view( 'product_variant.form', compact( 'row', 'saveUrl' ) );
	}

	public function save( Request $request ) {
		$request->validate([
			'size' => 'required',
			'price' => 'required',
			'color' => 'required'
		]);
		$data_save = [
			'product_id' => $request->product_id,
			'name' => $request->product . ' ' .$request->size,
			'size' => $request->size,
			'color' => implode( ",", $request->color ),
			'price' => $request->price,
		];
		if ( empty( $request->id ) ) {
			Product_variants::create( $data_save );
		} else {
			Product_variants::where( 'id', $request->id )->update( $data_save );		
		}
		return redirect( route( 'productsTable' ) );
	}

	public function delete( $id ) {
		Product_variants::where( 'id', $id )->delete();	
		return redirect( route( 'productsTable' ) );
	}
}
