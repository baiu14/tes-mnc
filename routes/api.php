<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get( 'categories/data', [CategoriesController::class, 'data'] )->name( 'categoriesData' );
Route::get( 'products/data', [ProductsController::class, 'data'] )->name( 'productsData' );

foreach( [
	'products' => ProductsController::class, 
	'categories' => CategoriesController::class ] as $m => $M ) {
	Route::post( $m . '/save', [ $M, 'save'] )->name( $m . 'Save' );
	Route::get( $m . '/delete/{id}', [ $M, 'delete'] )->name( $m . 'Delete' );
	Route::get( $m . '/get_detail/{id}', [ $M, 'get_detail'] )->name( $m . 'GetDetail' );
	Route::get( $m . '/get_all', [ $M, 'get_all'] )->name( $m . 'GetAll' );
}
Route::get( 'products/get_filter/{category}', [ ProductsController::class, 'get_filter'] )->name( 'productsGetFilter' );
Route::get( 'products/get_search/{keyword}', [ ProductsController::class, 'get_search'] )->name( 'productsGetSearch' );