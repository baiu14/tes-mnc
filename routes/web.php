<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProductVariantController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get( 'categories/table', [CategoriesController::class, 'table'] )->name( 'categoriesTable' );
// Route::get( 'categories/create', [CategoriesController::class, 'create'] )->name( 'categoriesCreate' );
// Route::post( 'categories/save', [CategoriesController::class, 'save'] )->name( 'categoriesSave' );
// Route::get( 'categories/edit/{id}', [CategoriesController::class, 'edit'] )->name( 'categoriesEdit' );
// Route::get( 'categories/delete/{id}', [CategoriesController::class, 'delete'] )->name( 'categoriesDelete' );
foreach( [
	'products' => ProductsController::class, 
	'categories' => CategoriesController::class ] as $m => $M ) {
	Route::get( $m . '/table', [ $M, 'table'] )->name( $m . 'Table' );
	Route::get( $m . '/detail/{id}', [ $M, 'detail'] )->name( $m . 'Detail' );
	Route::get( $m . '/create', [ $M, 'create'] )->name( $m . 'Create' );
	Route::get( $m . '/edit/{id}', [ $M, 'edit'] )->name( $m . 'Edit' );
}

Route::get( 'product_variant', [ ProductVariantController::class, 'index'] )->name( 'productVariant' );
Route::get( 'product_variant/create/{id}', [ ProductVariantController::class, 'create'] )->name( 'productVariantCreate' );
Route::post( 'product_variant/save', [ ProductVariantController::class, 'save'] )->name( 'productVariantSave' );
Route::get( 'product_variant/edit/{id}', [ ProductVariantController::class, 'edit'] )->name( 'productVariantEdit' );
Route::get( 'product_variant/delete/{id}', [ ProductVariantController::class, 'delete'] )->name( 'productVariantDelete' );